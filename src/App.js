import React, { Component } from 'react';
import classNames from 'classnames';
import logo from './logo.svg';
import './App.css';

const LENGTH_OF_PIN = 4;

class App extends Component {
  state = { saved_pin: '', pin: '', locked: false, message: '' };
  
  addDigit = (digit) => {
	  if (this.state.pin.length >= LENGTH_OF_PIN) return;
	  
	  var newState = Object.assign({}, this.state);
	  newState.pin = this.state.pin + digit;
	  
	  // clear message
	  newState.message = '';
	  
	  this.setState( newState );
  };
  
  addDigit0 = () => {
	  return this.addDigit(0);
  };

  addDigit1 = () => {
	  return this.addDigit(1);
  };

  addDigit2 = () => {
	  return this.addDigit(2);
  };

  addDigit3 = () => {
	  return this.addDigit(3);
  };

  addDigit4 = () => {
	  return this.addDigit(4);
  };

  addDigit5 = () => {
	  return this.addDigit(5);
  };

  addDigit6 = () => {
	  return this.addDigit(6);
  };

  addDigit7 = () => {
	  return this.addDigit(7);
  };

  addDigit8 = () => {
	  return this.addDigit(8);
  };

  addDigit9 = () => {
	  return this.addDigit(9);
  };
  
  clear = () => {
	  var newState = Object.assign({}, this.state);
	  newState.pin = '';
	  newState.message = '';
	  this.setState( newState );
  }
  
  submit = () => {
	  if(this.state.locked) {
		  if (this.state.pin === this.state.saved_pin) {
		  	// unlock
			  var newState = Object.assign({}, this.state);
			  newState.locked = false;
			  newState.pin = '';
			  this.setState( newState );
		  } else {
		  	// display invalid pin message
			  var newState = Object.assign({}, this.state);
			  newState.message = 'INVALID';
			  newState.pin = '';
			  this.setState( newState );
		  }
	  } else {
		  if (this.state.pin.length === LENGTH_OF_PIN) {
			  var newState = Object.assign({}, this.state);
			  newState.saved_pin = newState.pin;
			  newState.locked = true;
			  newState.pin = '';
			  this.setState( newState );
		  }
	  }
  }
  
  render() {
	var indicatorClasses = classNames({"state-indicator": true, "locked": this.state.locked});
    return (
      <div className="App container-fluid">
        <div className="safety-box">
		  <div className="safety-box-inner">
			<div className="row">
			  <div className="col-md-5">
				<div className="pin-pad-wrapper">
					<table className="pin-pad">
					  
					  <tr>
						<td><button onClick={this.addDigit1}>1</button></td>
						<td><button onClick={this.addDigit2}>2</button></td>
						<td><button onClick={this.addDigit3}>3</button></td>
					  </tr>
					  <tr>
						<td><button onClick={this.addDigit4}>4</button></td>
						<td><button onClick={this.addDigit5}>5</button></td>
						<td><button onClick={this.addDigit6}>6</button></td>
					  </tr>
					  <tr>
						<td><button onClick={this.addDigit7}>7</button></td>
						<td><button onClick={this.addDigit8}>8</button></td>
						<td><button onClick={this.addDigit9}>9</button></td>
					  </tr>
					  <tr>
						<td><button onClick={this.clear}>CLR</button></td>
						<td><button onClick={this.addDigit0}>0</button></td>
						<td><button onClick={this.submit}>⇨</button></td>
					  </tr>
					</table>
				</div>
			  </div>
			 <div className="col-md-6 col-md-offset-1">
				<div className="pin-display">
					<input value={this.state.message.length > 0 ? this.state.message : this.state.pin} readOnly />
				</div>
				<div className={indicatorClasses}></div>
			 </div>
			</div>
		  </div>
		</div>
      </div>
    );
  }
}

export default App;
